import React from "react";
import './App.css';
import HomeContainer from './HomeContainer';

const App = () => {

  return (  
    <HomeContainer /> 
  );
}

export default App; 