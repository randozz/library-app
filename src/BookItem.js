

const BookItem = ({ selectedIds }) => {

    function infobtn(id,author,bname,genre,pages,desc) {
        alert(
            "Book: " + bname + "\n" +
            "Genre: " + genre + "\n" +
            "Pages: " + pages + "\n" +
            "Author: " + author + "\n\n" +
             desc + "\n\n" +
             "ID: " + id
        );
    }

    function removebtn(id) {
        alert("Placeholder, remove id: " + id);
    }

    const listBooks = selectedIds.map((book) => 

    <div className="hl" key={book.id}>
        <div className="bookitems">
            
                <button id="icon" 
                        onClick={() => removebtn(book.id)}
                        >
                    &#10060;
                </button> 
                {book.bname}
           
        </div>

        <div className="bookitems">
            <button id="icon" onClick={() => infobtn(book.id,book.author,book.bname,book.genre,book.pages,book.desc)}>&#10132;</button>
        </div>
    </div>
    )

    return (
            <>
                {listBooks}
            </>
    )};

export default BookItem;