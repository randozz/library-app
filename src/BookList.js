import React from "react";

const BookList = ({ bdata, bookData, setBookData }) => {
    
    const addBook = (bid,author,bname,genre,pages,desc) => {
      setBookData(oldState => [...oldState, {
        id: bid,
        author: author,
        bname: bname,
        genre: genre,
        pages: pages,
        desc: desc
      }])
    }

    const removeBook = (bid) => {
      setBookData(bookData.filter(id => id.id !== bid));
    }

    const storeBookData = (bid,author,bname,genre,pages,desc) => {
      bookData.some((e) => e.id === bid)
      ? removeBook(bid)
      : addBook(bid,author,bname,genre,pages,desc);
    }

    const Marked = (bid) => { 
      const check = bookData.some((e) => e.id === bid)
        ? "laatikko mark"
        : "laatikko";

      return check;
    }

    const mappedBList = bdata.map((book) => {
        
        return (
            <div 
               className={Marked(Number(book.id))} 
               key={book.id} 
               onClick={() => storeBookData(book.id,book.author,book.bookname,book.genre,book.pages,book.description)}
            >
                 <div className="flexinbox2">
                   <div>
                   id: {book.id}
                   </div>
                    {/* <div className="bookinfo rivi">  */}
                    <div className="testi">
                        {book.bookname} by {book.author}
                    </div>
        
                    {/* </div> */}    
                  </div>              
            </div>         
        );
    })
    return mappedBList;              
};

export default BookList;