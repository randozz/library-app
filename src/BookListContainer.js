import React from "react";
import { useEffect, useState } from "react";
import mock_data from "./BOOK_DATA.json";
import BookList from "./BookList";

const BookListContainer = ({ text, type, bookData, setBookData }) => {
     
    const [data, setData] = useState([]);
    const rawBData = mock_data;

      useEffect(() => {
        setData(rawBData);
      },[rawBData])
    
      function SearchType() {
        switch (type) {
          case "id":{
            const arr_bdata = data.filter(book => book.id === Number(text));
            
            return (   
              <BookList bdata={arr_bdata} bookData={bookData} setBookData={setBookData} />
            );
        }
  
          case "author": {
            const arr_bdata = data.filter(book => book.author.toLowerCase().includes(text.toLowerCase()));
           
            return (   
              <BookList bdata={arr_bdata} bookData={bookData} setBookData={setBookData} />
            );
          }
  
          case "bookname": {
            const arr_bdata = data.filter(book => book.bookname.toLowerCase().includes(text.toLowerCase()));
            
            return (   
              <BookList bdata={arr_bdata} bookData={bookData} setBookData={setBookData} />
            );
          }
  
          case "genre": {
            const arr_bdata = data.filter(book => book.genre.toLowerCase().includes(text.toLowerCase()));
            
            return (   
              <BookList bdata={arr_bdata} bookData={bookData} setBookData={setBookData} />
            );
        } 
  
        default:
          return (   
            <BookList bdata={data} bookData={bookData} setBookData={setBookData} />
          );
        }
      }

    const switchTest = text !== null
      ? SearchType()
      : <BookList bdata={data}
                  bookData={bookData}
                  setBookData={setBookData}
        />;
    return switchTest;
    };

export default BookListContainer;