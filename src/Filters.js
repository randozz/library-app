import { useState } from "react";

function useFilters() {
    const [Find, setFind] = useState(null);
    const [findType, setfindType] = useState("id");

    return {
        Find,
        findType,

        render: (
            <>
                <div className="filtterit">
                    <input 
                        id="teksti"
                        type="text"
                        name="teksti" 
                        placeholder="Haku..."
                        onChange={(e) => setFind(e.target.value)} 
                    />
                
                    <select 
                        id="filtteri" 
                        name="filtteri"
                        onChange={(e) => setfindType(e.target.value)} 
                    >
                        <option value="id" defaultValue>ID</option>
                        <option value="author">Author</option>
                        <option value="bookname">Bookname</option>
                        <option value="genre">Genre</option>

                    </select>
                
                    <div className="testi">
                        <button onClick={() => setFind(null)}
                        >Reset</button>
                    </div>
                </div>
            </>
        )
    }    
};

export default useFilters;