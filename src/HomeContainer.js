import React, { useState } from "react";
import './App.css';
import BookListContainer from "./BookListContainer";
import BookItem from "./BookItem";
import useFilters from "./Filters";

const HomeContainer = () => { 
    const [ bookData, setBookData ] = useState([]);
    const { render, Find, findType } = useFilters();
   
    return (
        <div className="kontti">
            <div className="flexinbox">
                <div className="vasen">
                
                    {render} {/* Filtteri */}

                    <div className="bookitem">
                        <div>
                            Selected books: {bookData.length}
                        <div className="bookitem-right">
                            <button>Clear</button>
                        </div>
                    </div>

                    <hr />

                    <BookItem selectedIds={bookData} />  
                </div>
                
                </div>

                <div className="oikea">
                    <div className="rivi"> 

                        <BookListContainer 
                            text={Find} 
                            type={findType} 
                            bookData={bookData} 
                            setBookData={setBookData} 
                        />

                    </div>
                </div>
            </div>
        </div>
    )
};

export default HomeContainer;